<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeRegisterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_register')->insert(['name' => 'Email']);
        DB::table('type_register')->insert(['name' => 'Google']);
        DB::table('type_register')->insert(['name' => 'Facebook']);
    }
}
