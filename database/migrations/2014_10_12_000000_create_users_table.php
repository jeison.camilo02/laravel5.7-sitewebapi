<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('email', 50)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('profile_picture')->nullable();
            $table->date('birthday_date')->nullable();
            $table->bigInteger('cellphone');
            //relations
            $table->integer('type_register')->unsigned()->default(1); //Regsiter by email
            $table->integer('rol_id')->unsigned()->default(3); //Rol Student
            $table->integer('state_id')->unsigned()->default(1); //State

            $table->rememberToken();
            $table->timestamps();

            $table->foreign('type_register')->references('id')->on('type_register');
            $table->foreign('rol_id')->references('id')->on('roles');
            $table->foreign('state_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
