<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('certification_company');
            $table->date('expedition_date');
            $table->string('url');
            $table->timestamps();
        });


        Schema::create('request_certificate', function (Blueprint $table) {
            $table->increments('id');
            //relations
            $table->integer('request_id')->unsigned();
            $table->integer('certificate_id')->unsigned();
            $table->timestamps();
            $table->foreign('request_id')->references('id')->on('requests');
            $table->foreign('certificate_id')->references('id')->on('certificates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
