<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $table = "request";

    protected $fillable = ["name", "description", "type_request", "teacher_id"];


    public function type_request(){
        return $this->belongsTo('App\Type');
    }

    public function teacher(){
        return $this->belongsTo('App\User');
    }

    public function certificates(){
        return $this->belongsToMany('App\Certificate');
    }
}
