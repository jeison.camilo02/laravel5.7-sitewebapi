<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratings extends Model
{
    protected $table = "ratings";
    protected $fillable = ["score", "descrption", "grader_id", "teacher_id"];


    public function grader(){
        return $this->belongsTo('App\grader_id');
    }

    public function teacher(){
        return $this->belongsTo('App\teacher_id');
    }
}
