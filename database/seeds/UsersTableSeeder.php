<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas

        //Auto generate administrador
        DB::table('users')->insert([
            'name' => 'Administrador',
            'last_name' => 'Administrador',
            'email' => 'admin@email.com',
            'password' => bcrypt('123456'),
            'cellphone' => '3015262684',
            'rol_id' => '1'

        ]);

        //Auto generate some teacher
        DB::table('users')->insert([
            'name' => Str::random(10),
            'last_name' => Str::random(10),
            'email' => 'teacher@email.com',
            'password' => bcrypt('123456'),
            'cellphone' => '3015262684',
            'rol_id' => '2'

        ]);

        //Auto generate some student
        DB::table('users')->insert([
            'name' => Str::random(10),
            'last_name' => Str::random(10),
            'email' => 'student@email.com',
            'password' => bcrypt('123456'),
            'cellphone' => '3015262684',
            'rol_id' => '3',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]);

        //DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

    }
}
