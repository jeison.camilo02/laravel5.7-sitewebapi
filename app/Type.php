<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "Type";
    protected $fillable = ["name"];

    public function requests(){
        return $this->hasMany('App\Requests');
    }
}
