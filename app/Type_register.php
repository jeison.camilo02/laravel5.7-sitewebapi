<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_register extends Model
{
    protected $table = 'type_register';

    protected $fillable = ['name'];

    public function users(){
        return $this->hasMany('App\User');
    }
}
