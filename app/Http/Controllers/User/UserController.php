<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        foreach ($users as $key => $value) {
            $users[$key]->rol = $value->rol->name;
        }
        return response()->json(['data' => $users]);
    }

    public function store(Request $request)
    {
        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        return $user;
    }

    public function show($user)
    {
        $response = User::find($user);
        if($response == null) {
            return response()->json(['error' => 'Usuario no encontrado', 'code' => 404], 404);
        }
        return response()->json(['data' => $response]);
    }

    public function update(Request $request, User $user)
    {
        //
    }

    public function destroy(User $user)
    {
        //
    }
}
