<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table = "certificates";
    protected $fillable = ["name", "certification_company", "expedition_date", "url"];


    public function Requests(){
        return $this->belongsToMany('App\Request');
    }
}
