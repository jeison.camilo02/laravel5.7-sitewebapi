<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('score');
            $table->string('descrption', 500);
            $table->integer('grader_id')->unsigned(); //calificador->estudiante
            $table->integer('teacher_id')->unsigned(); //leccion
            $table->foreign('grader_id')->references('id')->on('users');
            $table->foreign('teacher_id')->references('id')->on('users');
            $table->timestamps();
        });


        Schema::create('teacher_student', function (Blueprint $table) {
            $table->increments('id');
            $table->date('initial_date');
            $table->date('final_date');
            $table->integer('student_id')->unsigned(); //calificador->estudiante
            $table->integer('teacher_di')->unsigned(); //leccion
            $table->foreign('student_id')->references('id')->on('users');
            $table->foreign('teacher_di')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
